The PE Internship Project 2020 is a data pipeline that ingests Twitter and polling data and generates statistics/visuals.

## Setup and Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
(** Not Complete **)
pip install pandas
pip install textblob
pip install apache-beam
#pip isntall apache-beam[gcp]
pip install tweepy

```

## Usage

Fill out the Credentials file using your own Twiter API keys.
```python
(This is a dummy example - we need to fill this out)
#run the twitter_stream
python twitter_stream.py
#append to database on an interval 
#(should this be in the streaming script?)
python twitter_etl.py

```
Import data from database into desired visualization suite (i.e. PowerBI).

## Contributing
Please create a new branch, commit your changes there, and then create a pull request to master with Lucas Oskorep and Nico Finelli added as reviewers  on it. 

## License
[MIT](https://choosealicense.com/licenses/mit/)