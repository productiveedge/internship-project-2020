#!/bin/bash

# check args
if [ "$1" == "" ]; then
    echo "enter command as: bash enableNLApi.sh [path_to_json_auth]"
    exit 1
fi

# enable natural language api
gcloud services enable language.googleapis.com

# auth key -- path to json key should be 1st arg
export GOOGLE_APPLICATION_CREDENTIALS=$1

# CALLS WOULD GO HERE 

# disable nl api
gcloud services disable language.googleapis.com
