#!/bin/bash

# A Bash Script for enabling AutoML APIs


#check arguments
if ["$1" == ""]
then
    echo "Enter your Google Application Cridentials"; read cred

    # enable AutoML API
   gcloud services enable automl.googleapis.com
  
   export GOOGLE_APPLICATION_CREDENTIALS = cred

else
     # enable AutoML API
   gcloud services enable automl.googleapis.com

   export GOOGLE_APPLICATION_CREDENTIALS = $1

fi