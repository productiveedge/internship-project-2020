import pandas as pd
import re

def gets_file():
    """
     Prompts the user for the file name and
     checks to see if file exists
     :param: None
     :return: None
    """
    user_input = input("Enter file name: ")
    try:
        f = open(user_input)
        f.close()
        add_sentiment(user_input)
    except FileNotFoundError:
        print("File not found")

def add_sentiment(file):
    """
    adds in the sentiment value for each tweet
    and saves the changes
    :param file: file name
    :return: none
    """
    datafile = pd.read_csv(file)
    if 'Sentiment' not in datafile:
        datafile['Sentiment'] = 'None'
    for ind, row in datafile.iterrows():
        if row['Sentiment'] == 'None':
            user_input = print_statement(row['text'])
            while not check_input(user_input):
                user_input = print_statement(row['text'])
            if user_input == '#':
                return
            else:
                datafile.iat[ind, 7] = user_input
                datafile.to_csv(file, index=False)

def check_input(user_input):
    """
    Checks the validity of the user input
    :param user_input:
    :return: bool
    """
    pattern = r"1|-1|0|#"
    if re.match(pattern, user_input):
        return True
    else:
        return False

def print_statement(text):
    """
    Prompts the user for a sentiment value
    :param text: string of text
    :return: integer value of user input
    """
    print(text)
    print()
    user_input = input(
        "Enter 1, -1, 0 for positive, negative or neutral sentiment respectively or # to "
        "quit:  ")
    return user_input

if __name__ == "__main__":
    gets_file()
