import tweepy as tw
import pandas as pd
import datetime as dt
import credentials
import processing
import constants

auth = tw.OAuthHandler(credentials.CONSUMER_KEY, credentials.CONSUMER_SECRET)
auth.set_access_token(credentials.ACCESS_TOKEN, credentials.ACCESS_TOKEN_SECRET)
api = tw.API(auth, wait_on_rate_limit=True)


def pull_tweets(search_term):
    """
    calls api to pull a specified number of tweets from the past week
    :return: an iterable tweets object
    """
    # specify: tweets about the two candidates from 7 days ago
    quantity = constants.search_terms[search_term]
    today = dt.date.today()
    start = today - dt.timedelta(days=7)
    end = today - dt.timedelta(days=6)

    try:
        tweets = tw.Cursor(api.search, q=search_term, lang="en", since=start, until=end).items(quantity)
    except Exception as ex:
        print(type(ex), " when pulling tweets")

    return tweets


def create_tweet_list():
    """
    handles filtering of each tweet from the pull (depending on place identifier)
    :return: list of processed tweet information for each tweet
    """
    try:
        tweets_info = []
        for search_term in constants.search_terms:
            print(search_term)
            tweets = pull_tweets(search_term)
            count = 0
            for tweet in tweets:
                count += 1
                print(count)
                data = tweet._json
                data_with_place = processing.filter_tweets(data)
                if data_with_place:  # tweet has a 'place' identifier with state
                    tweets_info.append(state_location_row(data_with_place, search_term))
                elif 'lang' in data and data['lang'] == 'en':
                    if data['user']['location'] != '':
                        tweets_info.append(custom_location_row(data, search_term))
    except tw.TweepError as e:
        print("tweepErr")
        print(e.args[0][0]['code'])
        print(e.args[0][0]['message'])
    except Exception as ex:
        print(type(ex), "occurred during proccessing")
    finally:
        return tweets_info


def get_full_text(tweet_id):
    """
    gets the full/extended text for the tweet -- some tweets in the pull are truncated
    :param tweet_id:
    :return: tweet's text with no newlines or returns
    """
    status = api.get_status(tweet_id, tweet_mode="extended")
    try:
        text = status.retweeted_status.full_text
    except AttributeError:  # Not a Retweet
        text = status.full_text

    return text.lower().replace('\n', ' ').replace('\r', '')


def state_location_row(data, keyword):
    """
    processing tweets that have a 'place' identifier -- adds 'state' value
    :param keyword: the search term used to get this tweet
    :param data: dictionary of tweet information to process
    :return: list of tweet information for the csv
    """
    data = processing.add_state_name(data)
    text = get_full_text(data['id'])
    return [data['created_at'], data['id'], data['user']['id'], data['user']['followers_count'], data['state'], text,
            keyword]


def custom_location_row(data, keyword):
    """
    processing tweets that have a custom user location
    :param keyword: the search term used to get his tweet
    :param data: dictionary of tweet information to process
    :return: list of tweet information for the csv
    """
    text = get_full_text(data['id'])
    location = data['user']['location'].replace('\n', ' ').replace('\r', '')
    return [data['created_at'], data['id'], data['user']['id'], data['user']['followers_count'],
            location, text, keyword]


def tweets_to_csv():
    """
    creates pandas dataframe from tweet info list, then pushes to csv
    """
    tweets_list = create_tweet_list()
    # create pandas dataframe from the list of tweets and headers
    tweets_df = pd.DataFrame(data=tweets_list,
                             columns=['created_at', 'tweet_id', 'user.id', 'user.followers_count',
                                      'location', 'text', 'search_word'])
    tweets_df.to_csv('twitter_historical.csv', index=False)


if __name__ == '__main__':
    tweets_to_csv()
