import os
import pandas as pd
from flask import Flask
from google.cloud import storage

app = Flask(__name__)


def get_poll_data():
    url = "https://projects.fivethirtyeight.com/polls-page/president_polls.csv"
    data = pd.read_csv(url,
                       usecols=['poll_id', 'state', 'pollster', 'fte_grade', 'start_date', 'end_date',
                                'candidate_name',
                                'pct'])

    data.to_csv("polls.csv", index=False)
    return True


def send_to_cloud(filename):
    storage_client = storage.Client.from_service_account_json("pe-2020-intern-project-key.json")
    # create a bucket object
    bucket = storage_client.get_bucket("pe-2020-intern-bucket")
    blob = bucket.blob(filename)
    blob.upload_from_filename(filename)
    os.remove("polls.csv")


@app.route('/')
def main():
    get_poll_data()
    send_to_cloud("polls.csv")
    return "Complete"


if __name__ == '__main__':
    app.run(debug=True)
