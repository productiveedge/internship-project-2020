import pandas as pd

# Two dictionaries that map US states to their abbreviations
us_state2_abbrev = {
    'Alabama': 'AL',
    'Alaska': 'AK',
    'American Samoa': 'AS',
    'Arizona': 'AZ',
    'Arkansas': 'AR',
    'California': 'CA',
    'Colorado': 'CO',
    'Connecticut': 'CT',
    'Delaware': 'DE',
    'District of Columbia': 'DC',
    'Florida': 'FL',
    'Georgia': 'GA',
    'Guam': 'GU',
    'Hawaii': 'HI',
    'Idaho': 'ID',
    'Illinois': 'IL',
    'Indiana': 'IN',
    'Iowa': 'IA',
    'Kansas': 'KS',
    'Kentucky': 'KY',
    'Louisiana': 'LA',
    'Maine': 'ME',
    'Maryland': 'MD',
    'Massachusetts': 'MA',
    'Michigan': 'MI',
    'Minnesota': 'MN',
    'Mississippi': 'MS',
    'Missouri': 'MO',
    'Montana': 'MT',
    'Nebraska': 'NE',
    'Nevada': 'NV',
    'New Hampshire': 'NH',
    'New Jersey': 'NJ',
    'New Mexico': 'NM',
    'New York': 'NY',
    'North Carolina': 'NC',
    'North Dakota': 'ND',
    'Northern Mariana Islands': 'MP',
    'Ohio': 'OH',
    'Oklahoma': 'OK',
    'Oregon': 'OR',
    'Pennsylvania': 'PA',
    'Puerto Rico': 'PR',
    'Rhode Island': 'RI',
    'South Carolina': 'SC',
    'South Dakota': 'SD',
    'Tennessee': 'TN',
    'Texas': 'TX',
    'Utah': 'UT',
    'Vermont': 'VT',
    'Virgin Islands': 'VI',
    'Virginia': 'VA',
    'Washington': 'WA',
    'West Virginia': 'WV',
    'Wisconsin': 'WI',
    'Wyoming': 'WY'
}
us_abbrev2_state = dict(map(reversed, us_state2_abbrev.items()))

def get_state_name(abbrev):
    if abbrev == '--':
        return ""
    else:
        return us_abbrev2_state[abbrev]

def get_poll_data():
    url = "https://docs.google.com/spreadsheets/d/e/2PACX" \
          "-1vQ56fySJKLL18Lipu1_i3ID9JE06voJEz2EXm6JW4Vh11zmndyTwejMavuNntzIWLY0RyhA1UsVEen0/pub?gid=0&single=true" \
          "&output=csv "
    data = pd.read_csv(url,
                       usecols=['url', 'state', 'pollster', 'start.date', 'end.date', 'biden', 'trump'])
    data.rename(columns={'url': 'poll_id', 'start.date': 'start_date', 'end.date': 'end_date'}, inplace=True)
    columns = ['poll_id', 'state', 'pollster', 'fte_grade', 'start_date', 'end_date', 'biden', 'trump']
    data = data.reindex(columns=columns)
    data['state'] = data.apply(lambda row: get_state_name(row['state']), axis=1, result_type='expand')
    data.to_csv("polls_economist.csv", index=False)


if __name__ == '__main__':
    get_poll_data()
