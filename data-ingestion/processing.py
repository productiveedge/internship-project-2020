import constants


def filter_tweets(data):
    wanted_keys = ['created_at', 'id', 'user']
    sub_data = {a: data[a] for a in wanted_keys if a in data}

    sub_data['place'] = ''
    if 'lang' in data and data['lang'] == 'en':
        if 'place' in data and data['place']:
            if 'country_code' in data['place'] and data['place']['country_code'] == 'US':
                sub_data['place'] = data['place']

    if sub_data['place']:
        return sub_data
    return False


def add_state_name(data):
    if data['place']['place_type'] == 'city':
        abbrev = data['place']['full_name'][-2:]
        data['state'] = constants.us_abbrev2_state[abbrev]
    elif data['place']['place_type'] == 'admin':
        data['state'] = data['place']['name']
    else:
        data['state'] = ""
    data.pop('place', None)
    return data


def identify_subject(text):
    if "trump" in text and "biden" in text:
        return "both"
    elif "biden" in text:
        return "biden"
    else:
        return "trump"


