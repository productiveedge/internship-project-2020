# import section includes all other files and libraries that would be
# used in this document  .

import tweepy
import Credential
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import json
import constants
import csv
import processing
from flask import Flask
from google.cloud import storage

app = Flask(__name__)


# Create a Stream Class
class TwitterStream:

    def stream_twitter(self, fetched_tweet_filename, search_term, amount):
        listener = MyStreamListener(fetched_tweet_filename, search_term, amount)
        auth = OAuthHandler(Credential.CONSUMER_KEY, Credential.CONSUMER_SECRET)
        auth.set_access_token(Credential.ACCESS_TOKEN, Credential.ACCESS_TOKEN_SECRET)

        stream = Stream(auth, listener, tweet_mode='extended')
        stream.filter(track=search_term, languages=['en'])
        stream.disconnect()


# Create a StreamListener
class MyStreamListener(tweepy.StreamListener):

    def __init__(self, fetched_tweet_filename, tag, amount):
        self.fetched_tweets_filename = fetched_tweet_filename
        self.tag = tag
        self.count = 0
        self.total = amount

    def on_error(self, status_code):
        """
        Checks to see if there is an error while retrieving data
        if so the status_code/error is printed.

        :param status_code: status of stream
        :return: boolean value
        """
        print(status_code)
        return False

    def on_data(self, data):
        """
        if data is successfully retrieved then data is processed

        :param data: data from stream
        :return: boolean value
        """
        try:
            raw_data = json.loads(data)
            if "retweeted_status" in raw_data:
                pass
            else:
                if raw_data['user']['location']:
                    if self.count < self.total:
                        final_data = self.break_down_data(raw_data, self.tag)
                        self.save_data(final_data)
                        self.count = self.count + 1
                        print("tweet saved")
                    else:
                        self.send_to_cloud(self.fetched_tweets_filename)
                        return False
            return True
        except BaseException as e:
            print("tweet not saved")

    def break_down_data(self, data, tag):
        """
        Data with location is further broken down to
        data with place or data with custom location

        :param tag: search key word
        :param data: data to break down from twitter stream
        :return decoded_data: compartmentalize twitter data
        """
        data_with_place = processing.filter_tweets(data)
        if data_with_place:
            decoded_data = self.process_data_with_place(data, data_with_place, tag)
        else:
            decoded_data = self.process_data_with_custom_location(data, tag)
        return decoded_data

    def process_data_with_place(self, data, data_with_place, tag):
        """
        Data processed in this function has a place

        :param tag: search key word
        :param data: place object
        :param data_with_place: compartmentalized twitter data
        :return decoded: tuple containing all relevant twitter info to be saved
        """
        data_with_place = processing.add_state_name(data_with_place)
        text = self.get_text(data)
        decoded = (data['created_at'], data['id'], data['user']['id'],
                   data['user']['followers_count'], data_with_place['state'], text, tag)
        return decoded

    def process_data_with_custom_location(self, data, tag):
        """
        Data processed in this function has a custom location

        :param data: compartmentalized twitter data and place object
        :param tag: contains the tag being search for
        :return decoded: tuple containing all relevant twitter info to be saved
        """
        location = data['user']['location'].replace('\n', ' ').replace('\r', '')
        text = self.get_text(data)
        decoded = (data['created_at'], data['id'], data['user']['id'],
                   data['user']['followers_count'], location, text, tag)
        return decoded

    def get_text(self, data_with_text):
        """
        Strips newline from text and converts to lower case

        :param data_with_text: text to transform
        :return text: transformed text
        """
        text = data_with_text["text"].lower().replace('\n', ' ').replace('\r', '')
        return text

    def save_data(self, data_to_be_saved):
        """
        Saves finalized data to a csv file in append mode

        :param data_to_be_saved: finalized to be saved
        :return: none
        """
        with open(self.fetched_tweets_filename, 'a', newline='', encoding='utf') as tf:
            write = csv.writer(tf)
            write.writerow(data_to_be_saved)
            tf.close()
        return True

    def send_to_cloud(self, filename):
        storage_client = storage.Client.from_service_account_json("pe-2020-intern-project-key.json")
        # create a bucket object
        bucket = storage_client.get_bucket("pe-2020-intern-bucket")
        blob = bucket.blob(filename)
        blob.upload_from_filename(filename)
        return "done"


@app.route("/")
def main():
    hash_tags_list = constants.search_terms
    fetched_tweets_filename = "twitter_stream.csv"
    if file_length(fetched_tweets_filename) == 0:
        with open("twitter_stream.csv", "w+") as file:
            writer = csv.writer(file)
            writer.writerow(
                ["created_at", "tweet_id", "user.id", "user.followers_count", "location", "text", "search_word"])
            file.close()
    for item in hash_tags_list:
        twitter_stream = TwitterStream()
        twitter_stream.stream_twitter(fetched_tweets_filename, item, hash_tags_list[item])
    return "complete"


if __name__ == "__main__":
    app.run(debug=True)
