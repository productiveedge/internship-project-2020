from nltk.sentiment.vader import SentimentIntensityAnalyzer
from textblob import TextBlob
from flair.models import TextClassifier
from flair.data import Sentence
import pandas as pd
import sklearn

# nltk.download('vader_lexicon')

# load flair classifier
classifier = TextClassifier.load('en-sentiment')

# sentiment constants
POSITIVE_SENTIMENT = "POSITIVE"
NEGATIVE_SENTIMENT = "NEGATIVE"
NEUTRAL_SENTIMENT = "NEUTRAL"


def nltk_sentiment(text):
    sid = SentimentIntensityAnalyzer()
    ss = sid.polarity_scores(text)
    sentiment_score = ss['compound']
    if sentiment_score > 0:
        sentiment = POSITIVE_SENTIMENT
    elif sentiment_score < 0:
        sentiment = NEGATIVE_SENTIMENT
    else:
        sentiment = NEUTRAL_SENTIMENT

    return sentiment, round(sentiment_score, 3)


def textblob_sentiment(text):
    tb = TextBlob(text)
    sentiment_score = tb.sentiment.polarity
    if sentiment_score > 0:
        sentiment = POSITIVE_SENTIMENT
    elif sentiment_score < 0:
        sentiment = NEGATIVE_SENTIMENT
    else:
        sentiment = NEUTRAL_SENTIMENT

    return sentiment, round(sentiment_score, 3)


def flair_sentiment(text):
    fl = Sentence(text)
    classifier.predict(fl)
    flair_sent = fl.labels[0].to_dict()
    if flair_sent['value'] == "NEGATIVE":
        sentiment_score = flair_sent['confidence'] * -1
    if flair_sent['value'] == "POSITIVE":
        sentiment_score = flair_sent['confidence']

    return flair_sent['value'], round(sentiment_score, 3)


def add_sentiment_models():
    tweet_df = pd.read_csv('labelled_twitter.csv', usecols=["text", "which_candidate", "Sentiment"])
    tweet_df[["nltk_sentiment", "nltk_confidence"]] = tweet_df.apply(lambda row: nltk_sentiment(row["text"]), axis=1,
                                                                     result_type='expand')
    tweet_df[["textblob_sentiment", "textblob_confidence"]] = tweet_df.apply(
        lambda row: textblob_sentiment(row["text"]), axis=1, result_type='expand')
    tweet_df[["flair_sentiment", "flair_confidence"]] = tweet_df.apply(lambda row: flair_sentiment(row["text"]), axis=1,
                                                                       result_type='expand')
    tweet_df.to_csv("twitter_models.csv", index=False)


def analyze_models():
    df = pd.read_csv("twitter_models.csv",
                     dtype={'text': str, 'which_candidate': str, 'Sentiment': float, 'nltk_confidence': float,
                            'textblob_confidence': float, 'flair_confidence': float})
    df = df[~df['Sentiment'].isnull()]
    df[['Sentiment']] = df[['Sentiment']].astype(int)

    print_metrics(df)


def print_metrics(data):
    models = ['nltk', 'textblob', 'flair']
    for model in models:
        if model == 'nltk':
            binary_labels, binary_scores, all_labels, equal_bounds, high_confidence = nltk_data(data)
        elif model == 'textblob':
            binary_labels, binary_scores, all_labels, equal_bounds, high_confidence = textblob_data(data)
        else:
            binary_labels, binary_scores, all_labels, equal_bounds, high_confidence = flair_data(data)
        print(model, "metrics:")
        print("binary (dropped all neutrals):")
        print(sklearn.metrics.classification_report(y_true=binary_labels, y_pred=binary_scores))
        print("equal bounds: neutral = -0.33 to 0.33:")
        print(sklearn.metrics.classification_report(y_true=all_labels, y_pred=equal_bounds))
        print("high confidence: neutral = -0.5 to 0.5:")
        print(sklearn.metrics.classification_report(y_true=all_labels, y_pred=high_confidence))


def nltk_data(data):
    # binary set
    no_neutrals = data[data['Sentiment'] != 0]
    data_binary = no_neutrals[no_neutrals['nltk_sentiment'] != NEUTRAL_SENTIMENT]
    binary_nltk_confidence = data_binary['nltk_confidence'].tolist()
    binary_scores = binary_round_scores(binary_nltk_confidence)
    binary_labels = data_binary['Sentiment'].tolist()

    # neutral included sets
    all_labels = data['Sentiment'].tolist()
    all_scores = data['nltk_confidence'].tolist()
    equal_bounds = equal_round_scores(all_scores)
    high_confidence = confidence_round_scores(all_scores)

    return binary_labels, binary_scores, all_labels, equal_bounds, high_confidence


def textblob_data(data):
    # binary set
    no_neutrals = data[data['Sentiment'] != 0]
    data_binary = no_neutrals[no_neutrals['textblob_sentiment'] != NEUTRAL_SENTIMENT]
    binary_textblob_confidence = data_binary['textblob_confidence'].tolist()
    binary_scores = binary_round_scores(binary_textblob_confidence)
    binary_labels = data_binary['Sentiment'].tolist()

    # neutral included sets
    all_labels = data['Sentiment'].tolist()
    all_scores = data['textblob_confidence'].tolist()
    equal_bounds = equal_round_scores(all_scores)
    high_confidence = confidence_round_scores(all_scores)

    return binary_labels, binary_scores, all_labels, equal_bounds, high_confidence


def flair_data(data):
    # binary set
    no_neutrals = data[data['Sentiment'] != 0]
    binary_flair_confidence = no_neutrals['flair_confidence'].tolist()
    binary_scores = binary_round_scores(binary_flair_confidence)
    binary_labels = no_neutrals['Sentiment'].tolist()

    # neutral included sets
    all_labels = data['Sentiment'].tolist()
    all_scores = data['flair_confidence'].tolist()
    equal_bounds = equal_round_scores(all_scores)
    high_confidence = confidence_round_scores(all_scores)

    return binary_labels, binary_scores, all_labels, equal_bounds, high_confidence


def binary_round_scores(scores):
    rounded_scores = []
    for score in scores:
        if score > 0:
            rounded_scores.append(1)
        else:
            rounded_scores.append(-1)
    return rounded_scores


def equal_round_scores(scores):
    rounded_scores = []
    for score in scores:
        if score >= 0.33:
            rounded_scores.append(1)
        elif score <= -0.33:
            rounded_scores.append(-1)
        else:
            rounded_scores.append(0)
    return rounded_scores


def confidence_round_scores(scores):
    rounded_scores = []
    for score in scores:
        if score >= 0.5:
            rounded_scores.append(1)
        elif score <= -0.5:
            rounded_scores.append(-1)
        else:
            rounded_scores.append(0)
    return rounded_scores


if __name__ == '__main__':
    add_sentiment_models()
    analyze_models()
