import sys
import codecs
import os
import pandas as pd
import apache_beam as beam
from apache_beam.io import ReadFromText
from apache_beam.io import WriteToText
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from google.cloud import bigquery
from google.oauth2 import service_account
import pandas_gbq
from pandas import DataFrame

#credentials = service_account.Credentials.from_service_account_file(
   # 'enter json file for credentials',
#)



""" options = PipelineOptions(
    flags=argv,
    runner='DataflowRunner',
    project='my-project-id',
    job_name='unique-job-name',
    temp_location='gs://my-bucket/temp',
    region='us-central1') """

# https://medium.com/@rajeshhegde/data-pipeline-using-apache-beam-python-sdk-on-dataflow-6bb8550bf366 how to run on dataflow

class transform_538(beam.DoFn): 
  # headers are - state,poll_id,pollster,fte_grade,start_date,end_date,candidate_name,pct
  def process(self, element):
    arr = element.split(",")
   
   #replace commas in pollster names with dash
    if(len(arr) == 9):
      arr[2] = arr[2] + "-" + arr[3]
      arr.pop(3)

    #Replace polls containing no state with "US"
    if arr[1] == "":
      arr[1] = "US"

    #add columns for Trump and Biden percent of vote
    #538 creates a row for each candidates voting percentage so 
    #for the rows with trump, biden is being given a value of 0 and vice versa
    #this makes it easier to separate biden and trump in the pre-merging stage
    if(arr[6] == 'Donald Trump'):
      arr.append(0)
      arr.append(float(arr[7]))
    elif(arr[6] == 'Joseph R. Biden Jr.'):
      arr.append(float(arr[7]))
      arr.append(0)
    else:
      arr.append(0)
      arr.append(0)
   
    return [{
      'state' : arr[1],
      'pollster' : arr[2],
      'start_date' : arr[4],
      'end_date' : arr[5],
      'biden' : float(arr[8]),
      'trump' : float(arr[9]),
        }]

class transform_economist(beam.DoFn):
  def process(self, element):
    arr = element.split(",")

    #replace comma in pollster names with dash
    if(len(arr) == 9):
      arr[2] = arr[2] + "-" + arr[3]
      arr.pop(3)
     
    #Replace polls containing no state with "US"
    if arr[1] == "":
      arr[1] = "US"

      
    #change dates in economist to match format of 538 dates
    arr[4] = arr[4].replace("2020","20")
    arr[5] = arr[5].replace("2020","20")
  

    return [{
      'state' : arr[1],
      'pollster' : arr[2],
      'start_date' : arr[4],
      'end_date' : arr[5],
      'biden' : float(arr[6]),
      'trump' : float(arr[7]),
    }]
class premerge_biden(beam.DoFn):
  def process(self,element):
    #format polls so that Biden's percent of votes can be grouped based on shared identifiers
    result = [((element['state'],element['pollster'],element['start_date'],element['end_date']),(element['biden']))]
    #since the transform stage gives 0 to the rows not mentioning biden they can be easily sorted out here
    if  element['biden'] == 0.0:
      return
    return result
class premerge_trump(beam.DoFn):
  def process(self,element):
    #format polls so that Trump's percent of votes can be grouped based on shared identifiers
    result = [((element['state'],element['pollster'],element['start_date'],element['end_date']),(element['trump']))]
    #since the transform stage gives 0 to the rows not mentioning trump they can be easily sorted out here
    if  element['trump'] == 0.0:
      return
    return result



class format_into_dictionary(beam.DoFn):
  def process(self,element):
    d = {
      'state' : element[0][0],
      'pollster' : element[0][1],
      'start_date' : element[0][2],
      'end_date' : element[0][3],
      #CoGroupByKey puts the biden and trump vote percentages in a dictionary
      'biden' : element[1]['biden'],
      'trump' : element[1]['trump'],
      'leading_candidate' : "",
    }

    return [d]
class average_polls_with_multiple_values_per_candidate(beam.DoFn):
  def process(self,element):
    #Some of the polls have multiple values for each candidate so this
    # class takes the average for each candidate in each poll
    sumTrump = 0
    sumBiden = 0
    for i in element['trump']:
      sumTrump += i
    for j in element['biden']:
      sumBiden += j
    #remove polls missing one of the candidates
    if len(element['trump']) == 0 or len(element['biden']) == 0:
      return
    element['trump'] = sumTrump / (len(element['trump']))
    element['biden'] = sumBiden / (len(element['biden']))
    element['trump'] = round(element['trump'],2)
    element['biden'] = round(element['biden'],2)
    return [element]
class add_leading_candidate_column(beam.DoFn):
  def process(self, element):
    #set column for leading candidate based off of which candidate got
    # a higher percentage of the votes
    if element['biden'] > element['trump']:
      element['leading_candidate'] = 'Biden'
    elif element['trump'] > element['biden']:
      element['leading_candidate'] = 'Trump'
    else:
      element['leading_candidate'] = 'Tie'

    listFormat = [element['state'],element['pollster'],element['start_date'],element['end_date'],str(element['biden']),str(element['trump']),element['leading_candidate']]
    csvFormat = [','.join(listFormat)]
    return csvFormat
    #return [element]

table = 'Enter table name'
project_id = 'Enter project id'
dataset_id = 'enter dataset id'
table_id = 'enter table id'
table_schema = 'state:STRING, pollster:STRING, start_date:STRING, end_date:STRING, biden:FLOAT, trump:FLOAT, leading_candidate:STRING'
def run_etl(fivethirtyeight,economist):
  p = beam.Pipeline(options = PipelineOptions())
  csv_lines538 = (
    p | 'readmyfile' >> ReadFromText(fivethirtyeight,skip_header_lines=1)
    | 'clean' >> beam.ParDo(transform_538())
  )
  csv_linesEconomist = (
    p | 'readmyfile2' >> ReadFromText(economist,skip_header_lines=1) 
    | 'clean2' >> beam.ParDo(transform_economist())
  )
  merged = ((csv_lines538,csv_linesEconomist) | 'merge PCollections' >> beam.Flatten()
  )
  biden = (
    merged | 'pre-mergeBiden' >> beam.ParDo(premerge_biden())
  )
  trump = (
    merged | 'pre-mergeTrump' >> beam.ParDo(premerge_trump())
  )
  output = (
    {
      'biden': biden,
      'trump': trump
    }
    | 'merge538' >> beam.CoGroupByKey()
    | 'format538' >> beam.ParDo(format_into_dictionary())  
    | 'average' >> beam.ParDo(average_polls_with_multiple_values_per_candidate()) 
    | 'addColumn' >> beam.ParDo(add_leading_candidate_column())
  )
  if(local_mode):
    write = (
      output | 'write' >> WriteToText("ETL/merged-polling-data", file_name_suffix=".csv", shard_name_template='',header='state,pollster,start_date,end_date,biden,trump,leading_candidate')
    )
  else:
    write = (
    output | 'write' >> beam.io.WriteToBigQuery(
    table=table_id,
    dataset=dataset_id,
    project=project_id,
    schema=table_schema,
    create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND 
      )
    )
  p.run()
  



  # beam writetotext doesnt seem to support appending to a file, only to bigquery tables
  




if __name__ == "__main__":
  fivethirtyeight = "ETL/polls.csv"
  economist = "ETL/polls_economist.csv"
  if (sys.argv[1] == "local") & (len(sys.argv) == 2):
    local_mode = True
  elif (sys.argv[1] == "cloud") & (len(sys.argv) == 6):
    local_mode = False
  else:
    print("Local Usage: python polling-etl.py local")
    print("Streaming Usage: python polling-etl.py streaming --key [PATH_TO_KEY] --project [GCP_PROJECT_ID]")
    sys.exit(0)
  run_etl(fivethirtyeight,economist)


