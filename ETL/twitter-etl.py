import sys
import re
import codecs
import os
import pandas as pd
from regex_patterns import *
from dicts import *
from table_settings import *
from csv import reader
from io import StringIO
import apache_beam as beam
from apache_beam.io import ReadFromText
from apache_beam.io import WriteToText
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from flair.models import TextClassifier
from flair.data import Sentence
from datetime import datetime

classifier = TextClassifier.load('en-sentiment')

def get_sentiment_bucket(sentiment):
  return round(round(sentiment / 0.05) * 0.05, 2)

def get_pos_or_neg(sentiment):
  if sentiment > 0:
    return "positive"
  return "negative"

def get_sentiment(text):
  fl = Sentence(text)
  classifier.predict(fl)
  flair_sent = fl.labels[0].to_dict()
  if flair_sent['value'] == "NEGATIVE":
      return flair_sent['confidence'] * -1
  else:
      return flair_sent['confidence']

#extract correctly formatted state, otherwise empty string
def state_extract(text_in):
  for pattern in state_patterns:
    stateRegex = re.compile(pattern)
    stateMatch = stateRegex.search(text_in)
    if stateMatch: 
      if (',' in stateMatch.group()):
          index = stateMatch.group().find(',')
          text_in = stateMatch.group()[index+1:]
          text_in = text_in.lstrip()
      else: 
          text_in = stateMatch.group()
      return text_in
  return ""

def get_candidate_about(tweet_text, search_term):
  lower_text = tweet_text.lower()
  if search_term in trump_terms:
    for term in biden_terms:
      if term.lower() in lower_text:
        return "Both"
    return "Trump"
  if search_term in biden_terms:
    for term in trump_terms:
      if term.lower() in lower_text:
        return "Both"
    return "Biden"
  return "Not Found"

#quotation workaround - csv reader doesnt behave correctly when more than two double quotes
def fix_quotes(text_in):
  text_in = text_in.replace('"', "")
  text_in = "\"" + text_in + "\""
  return text_in

def clean_row(vals, isLocal):
  created_at, tweet_id, user_id, follower_count, location, tweet_text, search_word = vals
  #headers vs nonheaders
  if created_at == "created_at":
    tweet_sentiment = "tweet_sentiment"
    candidate_about = "candidate_about" 
    sentiment_bucket = "sentiment_bucket"
    pos_or_neg_sentiment = "pos_or_neg_sentiment" 
    date = "date"
  else: 
    # ensure valid state, otherwise state is empty
    location = state_extract(location)
    if location == "": return

    #scrap tweets that talk about both
    candidate_about = get_candidate_about(tweet_text, search_word)
    if candidate_about == "Both": return

    #replace abbreviations with full name
    if location in state_abbrev:
      location = state_abbrev[location]
    tweet_sentiment = get_sentiment(tweet_text)
    tweet_text = fix_quotes(tweet_text)

    #add column with created_at as type datetime
    date = datetime.strftime(datetime.strptime(created_at,'%a %b %d %H:%M:%S +0000 %Y'), '%Y-%m-%d %H:%M:%S')

    sentiment_bucket, pos_or_neg_sentiment = get_sentiment_bucket(tweet_sentiment), get_pos_or_neg(tweet_sentiment)


  if isLocal:
    joined_string = ",".join([created_at, tweet_id, user_id, 
                            follower_count, location, tweet_text, 
                            str(tweet_sentiment), str(sentiment_bucket), pos_or_neg_sentiment,
                            search_word, candidate_about,date])
    return [joined_string]
  else:
    #currently not up to date with local
    return [{"date": str(created_at), 
                  "tweet_id": int(tweet_id),
                  "user_id": int(user_id),
                  "user_followers_count": int(follower_count),
                  "state": str(location),
                  "text": str(tweet_text),
                  "tweet_sentiment": float(tweet_sentiment),
                  "search_word": str(search_word),
                  "cadidate_about": str(candidate_about),
                  "date": date}]
  
class transform_local(beam.DoFn): 
  # original headers are - created_at,tweet_id,user.id,user.followers_count,location,text,which_candidate
  def process(self, element):
    data = StringIO(element)
    row = reader(data, delimiter=',')
    isLocal = True
    #workaround for reading columns as beam read row by row
    for vals in row: 
      return clean_row(vals, isLocal)
        

class transform_cloud(beam.DoFn): 
  # original headers are - created_at,tweet_id,user.id,user.followers_count,location,text,which_candidate
  def process(self, element):
    data = StringIO(element)
    row = reader(data, delimiter=',')
    isLocal = False
    #workaround for reading columns as beam read row by row
    for vals in row: 
      return clean_row(vals, isLocal)

def run_local_etl(file_in, file_out):
  #runs the pipeline
  p = beam.Pipeline(options = PipelineOptions())
  _ = (
    p | 'readmyfile' >> ReadFromText(file_in)
    | 'clean' >> beam.ParDo(transform_local())
    # beam writetotext doesnt seem to support appending to a file, only to bigquery tables
    | 'write' >> WriteToText("temp", file_name_suffix=".csv", shard_name_template='')
  )
  p.run()

  #merges to database/big csv
  df_in = pd.read_csv("temp.csv")
  df_all = pd.read_csv(file_out)
  
  merged = pd.concat([df_in, df_all])
  merged = merged.drop_duplicates(subset='tweet_id', keep='last')
  merged.to_csv(file_out, index=False)

  os.remove("temp.csv")
  os.remove(file_in)

def run_cloud_etl(file_in):
  #runs the pipeline
  p = beam.Pipeline(options = PipelineOptions())
  data_from_source = (
    p | 'readmyfile' >> ReadFromText(file_in)
    | 'clean' >> beam.ParDo(transform_cloud())
  )

  data_from_source | 'write' >> beam.io.WriteToBigQuery(
    table=table_id,
    dataset=dataset_id,
    project=project_id,
    schema=table_schema,
    create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
    batch_size=int(100)
  )

  result = p.run()
  result.wait_until_finish()

## required streaming commands
# set GOOGLE_APPLICATION_CREDENTIALS=C:\Users\cylee\Documents\cloud_test\internship-project-2020\local-beam\credentials.json
# python twitter-etl.py --key C:\Users\cylee\Documents\cloud_test\internship-project-2020\local-beam\credentials.json --project casual-calculus-279816

if __name__ == "__main__":
  
  files_in = {"twitter-in.csv"}
  file_out = "twitter-out-all.csv"
  if (sys.argv[1] == "local") & (len(sys.argv) == 2):
    local_mode = True
  elif (sys.argv[1] == "streaming") & (len(sys.argv) == 6):
    local_mode = False
  else:
    print("Local Usage: python twitter-etl.py local")
    print("Streaming Usage: python twitter-etl.py streaming --key [PATH_TO_KEY] --project [GCP_PROJECT_ID]")
    sys.exit(0)

  for file_in in files_in:
    if local_mode: 
      run_local_etl(file_in, file_out)
    else: 
      run_cloud_etl(file_in)
    

