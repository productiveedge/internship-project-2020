import pandas as pd
from datetime import datetime

ViewTimeline = "14day"
df = pd.read_csv("ETL/twitter-out-all.csv")
df['date'] = pd.to_datetime(df['date'])
df = df[df.date > datetime.now() - pd.to_timedelta(ViewTimeline)]
df.to_csv("View_twitter_data.csv",index = False)