import pandas as pd
import os as os
import nltk
from wordcloud import WordCloud
import matplotlib.pyplot as plt
nltk.download('stopwords')
import numpy as np

#view all columns
pd.set_option('display.max_columns', None)

#make sure to update your working directory to the correct one-this is my local one, and it won't
#work on your machine
os.chdir('/Users/maitri/PycharmProjects/intern1')
#read in data
twitter_df = pd.read_csv('HistTweetsFixed.csv')
#get tweet text content from pandas dataframe column
list_of_strings = list(twitter_df['text'])
#convert list of strings into one giant string of text
listToStr = ' '.join(map(str, list_of_strings))
#stop words
#remove stop words (predefined set and custom appended ones)
stop_words = set(stopwords.words('english'))
stop_words.update(['https','co','hi'])
#stemming
#run, running, runner
#generate word cloud
#v2 will be to generate the flag image word cloud
wordcloud = WordCloud(stopwords=stop_words).generate(listToStr)
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")
plt.show()