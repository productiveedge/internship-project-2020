import pandas as pd
import os as os
import nltk
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import matplotlib.pyplot as plt
nltk.download('stopwords')
import numpy as np
from PIL import Image
#View all columns, getting data, and reading in the data
pd.set_option('display.max_columns', None)
#My working directory
os.chdir('/Users/M/PycharmProjects/wordcloud/')
#Read in data
df = pd.read_csv('C:/Users/M/Documents/HistTweetsFixed.csv')
#Get tweet text content from pandas dataframe column
list_of_strings = list(df['text'])
#Convert list of strings into one giant string of text
listToStr = ' '.join(map(str, list_of_strings))
#Remove stop words (predefined set and custom appended ones)
stop_words = set(STOPWORDS)
stop_words.update(['https', 'co','hi', 'amp', 'to', 'is', 'this', 'it', 'of', 'donald', 'joe', 'trump', 'biden', 'realdonaldtrump', 'president'])
mask = np.array(Image.open("flag_mask1.png"))
#Generate word cloud
#V2 will be to generate the flag image word cloud
wordcloud = WordCloud(stopwords=stop_words,
                      background_color= "white",
                      max_words= 1000,
                      mode="RGBA",
                      mask=mask,
                      max_font_size=45,
                      min_font_size=5
                      ).generate(listToStr)
#Create coloring from image
image_colors=ImageColorGenerator(mask)
plt.figure(figsize=[7, 7])
plt.imshow(wordcloud.recolor(color_func=image_colors), interpolation='bilinear')
plt.axis("off")
#Store to file
plt.savefig("flag_mask1.png", format="png")
plt.show()