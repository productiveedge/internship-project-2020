import pandas as pd
import os as os
import nltk
nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
import string
import re


#view all columns
#pd.set_option('display.max_columns', None)

#make sure to update your working directory to the correct one-this is my local one, and it won't
#work on your machine
os.chdir('C:/Users/Admin/Desktop/twitter-out-all-4')
#read in data
twitter_df = pd.read_csv('HistTweetsFixed.csv')
#get tweet text content from pandas dataframe column
list_of_strings = list(twitter_df['text'])
#convert list of strings into one giant string of text
listToStr = ' '.join(map(str, list_of_strings))
#stop words
#remove stop words (predefined set and custom appended ones)
stop_words = set(stopwords.words('english'))
stop_words.update(['https','co','hi','lol','.',',','"',';',':','@','(',')'])
listToStr_split = listToStr.split()
resultwords = [word for word in listToStr_split if word.lower() not in stop_words]
result = ' '.join(resultwords).split()

#stemming
ps = PorterStemmer()
list_stem = [ps.stem(word) for word in result]

#remove numeric strings (i.e. 1, 2, 3, 4)
def remove(list):
    pattern = '[0-9]'
    list = [re.sub(pattern, '', i) for i in list]
    return list

list_stem = remove(list_stem)

#remove punctuation
list_stem = [''.join(c for c in s if c not in string.punctuation) for s in list_stem]

#convert list of word cloud words to pandas data frame column
word_cloud_df = pd.DataFrame({'word_cloud_words': list_stem})